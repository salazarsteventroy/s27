const http = require('http');

let directory = [
	{
"firstName": "Mary Jane",
"lastName": "Dela Cruz",
"mobileNo": "09123456789",
"email": "mjdelacruz@mail.com",
"password": 123
},
{
"firstName": "John",
"lastName": "Doe",
"mobileNo": "09123456789",
"email": "jdoe@mail.com",
"password": 123
}
]
http.createServer((request, response) => {
	if(request.url == '/profile' && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(directory));
		
		response.end()
	}

	if(request.url == "/profile" && request.method == "POST"){
		let requestBody = ''
		request.on('data', function(data){
			requestBody += data;
		})

		request.on('end', function(){
			
			console.log(typeof requestBody)

			
			requestBody = JSON.parse(requestBody)

			let newUser = {
				"firstName": requestBody.firstName,
				"lastName": requestBody.lastName,
				"mobileNo": requestBody.mobileNo,
				"email": requestBody.email,
				"password": requestBody.password
			}

			directory.push(newUser)
			console.log(directory)

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(newUser));
			response.end()
		})


	}

}).listen(4000)

console.log('Server running at localhost:4000')